# PicoToolBox

This document shows the data structures in PicoToolBox.
While not extensively covering parsing, it should allow you to better understand the structure in case you want to create your own data analysis.

## Picoscenes

The `Picoscenes` class contains all frames read from a file. Thourgh the `raw` member list, you can access single frames,
for example:

```python
frames = Picoscenes("rx_by_usrpN210.csi") # return a Picoscenes Object
First_Frame = frames.raw[0] # get the first frame (change index to get any other)
```

The following presents more details of the single frames, with accompanying small examples.

### Frame

The `Frame` is a structure which inlcudes all information and structure.
In case the docs are not up-to-date, you may check `print(frames.raw[0])` for the complete dictionary
of information.

Some attributes are **optional**, meaning they may be absent in the frame dictionary.
This is directly translated from the underlying cpp code, where the optional values may
be None, as can be checked by their `has_value()` methods.

| Name             | Type                       | Illustration |
| ---------------- | -------------------------- | ------------ |
| StandardHeader   | ieee80211_mac_frame_header |              |
| RxSBasic         | RxSBasicSegment            |              |
| RxExtraInfo      | ExtraInfoSegment           |              |
| CSI              | CSISegment                 |              |
| MVMExtra         | MVMExtraSegment            | optional     |
| PicoScenesHeader | PicoScenesFrameHeader      | optional     |
| TxExtraInfo      | ExtraInfoSegment           | optional     |
| PilotCSI         | CSISegment                 | optional     |
| LegacyCSI        | CSISegment                 | optional     |
| BasebandSignals  | BasebandSignalSegment      | optional     |
| PreEQSymbols     | PreEQSymbolsSegment        | optional     |
| MPDU             | vector[uint8_t]            |              |


### CSI

CSI is the most important structure in Picoscenes.

| Name                | Type                           | Illustration   |
| ------------------- | ------------------------------ | -------------- |
| DeviceType          | PicoScenesDeviceType           | uint16_t       |
| PacketFormat        | PacketFormatEnum               | int8_t         |
| CBW                 | ChannelBandwidthEnum           | uint16_t       |
| CarrierFreq         | uint64_t                       |                |
| SamplingRate        | uint64_t                       |                |
| SubcarrierBandwidth | uint32_t                       |                |
| numTones            | CSIDimension                   | uint16_t       |
| numTx               | CSIDimension                   | uint8_t        |
| numRx               | CSIDimension                   | uint8_t        |
| numESS              | CSIDimension                   | uint8_t        |
| numCSI              | CSIDimension                   | uint16_t       |
| ant_sel             | uint8_t                        |                |
| CSI                 | SignalMatrix[ccomplex[double]] | Cplx structure |
| Mag                 | SignalMatrix[double]           | Cplx structure |
| Phase               | SignalMatrix[double]           | Cplx structure |
| SubcarrierIndex     | vector[int16_t]                | Cplx structure |

**uint** means **unsigned int**

#### Example 

To read the CSI from, for example, the first frame, you can use:

```python
frames = Picoscenes("rx_by_usrpN210.csi")
frame = frames.raw[0]
n_rx_antennas=frame.get("RxSBasic").get("numRx")
n_streams=frame.get("RxSBasic").get("numSTS")
n_tones=frame.get("CSI").get("numTones")

# CSI is stored in flattened MATLAB (n_tones, n_streams, n_rx_antennas) format
csi = frame.get("CSI").get("CSI").reshape((n_tones, n_streams, n_rx_antennas), order="F")
```

#### PicoScenesDeviceType (uint16_t)

        # all attributes are uint16_t
        QCA9300
        IWL5300
        IWLMVM
        MAC80211Compatible
        USRP
        VirtualSDR
        Unknown


#### PacketFormatEnum(int8_t)

        PacketFormat_NonHT
        PacketFormat_HT
        PacketFormat_VHT
        PacketFormat_HESU
        PacketFormat_HEMU
        PacketFormat_Unknown


#### ChannelBandwidthEnum(uint16_t)

        CBW_5
        CBW_10
        CBW_20
        CBW_40
        CBW_80
        CBW_160


#### SignalMatrix

| Name       | Type            | Illustratio |
| ---------- | --------------- | ----------- |
| array      | vector[T]       |             |
| dimensions | vector[int64_t] |             |


#### CSIDimension

        uint16_t numTones
        uint8_t numTx
        uint8_t numRx
        uint8_t numESS
        uint16_t numCSI

### StandardHeader

| Name         | Type                       | Illustration   |
| ------------ | -------------------------- | -------------- |
| ControlField | ieee80211_mac_frame_header | Cplx Structure |
| Addr1        | uint8_t [6]                |                |
| Addr2        | uint8_t [6]                |                |
| Addr3        | uint8_t [6]                |                |
| Fragment     | uint16_t                   |                |
| Sequence     | uint16_t                   |                |

#### Example 

```python
i = 0  # stands for the first frame of csi frames

frames = Picoscenes("rx_by_usrpN210.csi")
fragment = frames.raw[i].get("StandardHeader").get("Fragment")
```

#### ieee80211_mac_frame_header

| Name       | Type     | Illustration |
| ---------- | -------- | ------------ |
| version    | uint16_t |              |
| type       | uint16_t |              |
| subtype    | uint16_t |              |
| toDS       | uint16_t |              |
| fromDS     | uint16_t |              |
| moreFrags  | uint16_t |              |
| retry      | uint16_t |              |
| power_mgmt | uint16_t |              |
| more       | uint16_t |              |
| protect    | uint16_t |              |
| order      | uint16_t |              |


### RxSBasic

| Name         | Type     | Illustration |
| ------------ | -------- | ------------ |
| deviceType   | uint16_t |              |
| timestamp    | uint64_t |              |
| centerFreq   | int16_t  |              |
| controlFreq  | int16_t  |              |
| CBW          | uint16_t |              |
| packetFormat | uint8_t  |              |
| packetCBW    | uint16_t |              |
| GI           | uint16_t |              |
| MCS          | uint8_t  |              |
| numSTS       | uint8_t  |              |
| numESS       | uint8_t  |              |
| numRx        | uint8_t  |              |
| noiseFloor   | int8_t   |              |
| rssi         | int8_t   |              |
| rssi1        | int8_t   |              |
| rssi2        | int8_t   |              |
| rssi3        | int8_t   |              |


#### Example 

```python	
i = 0  # stands for the first frame of csi frames
frames = Picoscenes("rx_by_usrpN210.csi")
timestamp = frames.raw[i].get("RxSBasic").get("timestamp")
```


### RxExtraInfo(Optional)

`RxExtraInfo` is also a complex flexible structure. It contains the **has_xxx** attribute.

**has_xxx** is a **bint** type, which is 0 or 1. 

For example, if **hasLength** == 1 ,then **RxExtraInfo.Length** is a concrete value, or it is **None** 

In order to **not** trouble you, I will just list the meaningful information of this struct.


| Name             | Type                  | Illustration   |
| ---------------- | --------------------- | -------------- |
| length           | uint16_t              |                |
| version          | uint64_t              |                |
| macaddr_cur      | uint8_t [6]           |                |
| macaddr_rom      | uint8_t [6]           |                |
| chansel          | uint32_t              |                |
| bmode            | uint8_t               |                |
| evm              | int8_t [20]           |                |
| tx_chainmask     | uint8_t               |                |
| rx_chainmask     | uint8_t               |                |
| txpower          | uint8_t               |                |
| cf               | uint64_t              |                |
| txtsf            | uint32_t              |                |
| last_txtsf       | uint32_t              |                |
| channel_flags    | uint16_t              |                |
| tx_ness          | uint8_t               |                |
| tuning_policy    | AtherosCFTuningPolicy | Cplx structure |
| pll_rate         | uint16_t              |                |
| pll_clock_select | uint8_t               |                |
| pll_refdiv       | uint8_t               |                |
| agc              | uint8_t               |                |
| ant_sel          | uint8_t [3]           |                |
| cfo              | int32_t               |                |
| sfo              | int32_t               |                |



#### Example 

```python	
i = 0  # stands for the first frame of csi frames

frames = Picoscenes("rx_by_usrpN210.csi")
if frames.raw[i].get("RxExtraInfo", {}).get("has_txpower"):
    txpower = frames.raw[i].get("RxExtraInfo", {}).get("txpower")
    # make sure the frame has_xxx attribute then assign the variable
```


#### AtherosCFTuningPolicy

| Name                    | Type    | Illustration |
| ----------------------- | ------- | ------------ |
| CFTuningByDefault       | uint8_t |              |
| CFTuningByChansel       | uint8_t |              |
| CFTuningByFastCC        | uint8_t |              |
| CFTuningByHardwareReset | uint8_t |              |


### MVMExtra(Optional)

| Name       | Type     | Illustration |
| ---------- | -------- | ------------ |
| FMTClock   | uint32_t |              |
| usClock    | uint32_t |              |
| RateNFlags | uint32_t |              |


### PicoScenesHeader(Optional)

| Name       | Type                 | Illustration |
| ---------- | -------------------- | ------------ |
| MagicValue | uint32_t             |              |
| Version    | uint32_t             |              |
| DeviceType | PicoScenesDeviceType | uint16_t     |
| FrameType  | uint8_t              |              |
| TaskId     | uint16_t             |              |
| TxId       | uint16_t             |              |


### TxExtraInfo(Optional)

`TxExtraInfo` is also a complex flexible structure. It contains the **has_xxx** attribute.

**has_xxx** is a **bint** type, which is 0 or 1. 

For example, if **hasLength** == 1 ,then **Txtrainfo .Length** is a concrete value, or it is **None** 

In order to **not** trouble you, I will just list the meaningful information of this struct.

| Name             | Type                  | Illustration   |
| ---------------- | --------------------- | -------------- |
| length           | uint16_t              |                |
| version          | uint64_t              |                |
| macaddr_cur      | uint8_t [6]           |                |
| macaddr_rom      | uint8_t [6]           |                |
| chansel          | uint32_t              |                |
| bmode            | uint8_t               |                |
| evm              | int8_t [20]           |                |
| tx_chainmask     | uint8_t               |                |
| rx_chainmask     | uint8_t               |                |
| txpower          | uint8_t               |                |
| cf               | uint64_t              |                |
| txtsf            | uint32_t              |                |
| last_txtsf       | uint32_t              |                |
| channel_flags    | uint16_t              |                |
| tx_ness          | uint8_t               |                |
| tuning_policy    | AtherosCFTuningPolicy | Cplx structure |
| pll_rate         | uint16_t              |                |
| pll_clock_select | uint8_t               |                |
| pll_refdiv       | uint8_t               |                |
| agc              | uint8_t               |                |
| ant_sel          | uint8_t [3]           |                |
| cfo              | int32_t               |                |
| sfo              | int32_t               |                |


#### Example 

```python	
i = 0  # stands for the first frame of csi frames

frames = Picoscenes("rx_by_qca9300.csi")
txpower = frames.raw[i].get("TxExtraInfo", {}).get("txpower")
print(txpower)
```


### PilotCSI(Optional)

| Name                | Type                           | Illustration   |
| ------------------- | ------------------------------ | -------------- |
| DeviceType          | PicoScenesDeviceType           | uint16_t       |
| PacketFormat        | PacketFormatEnum               | int8_t         |
| CBW                 | ChannelBandwidthEnum           | uint16_t       |
| CarrierFreq         | uint64_t                       |                |
| SamplingRate        | uint64_t                       |                |
| SubcarrierBandwidth | uint32_t                       |                |
| numTones            | CSIDimension                   | uint16_t       |
| numTx               | CSIDimension                   | uint8_t        |
| numRx               | CSIDimension                   | uint8_t        |
| numESS              | CSIDimension                   | uint8_t        |
| numCSI              | CSIDimension                   | uint16_t       |
| ant_sel             | uint8_t                        |                |
| CSI                 | SignalMatrix[ccomplex[double]] | Cplx structure |
| Mag                 | SignalMatrix[double]           | Cplx structure |
| Phase               | SignalMatrix[double]           | Cplx structure |
| SubcarrierIndex     | vector[int16_t]                | Cplx structure |


#### Example 

```python
i = 0
frames = Picoscenes("rx_by_usrpN210.csi")
frame = frames.raw[i]  
if frame.get("PilotCSI"):
    num_tones = frame.get("PilotCSI", {}).get("numTones")
```


### LegacyCSI(Optional)

| Name                | Type                           | Illustration   |
| ------------------- | ------------------------------ | -------------- |
| DeviceType          | PicoScenesDeviceType           | uint16_t       |
| PacketFormat        | PacketFormatEnum               | int8_t         |
| CBW                 | ChannelBandwidthEnum           | uint16_t       |
| CarrierFreq         | uint64_t                       |                |
| SamplingRate        | uint64_t                       |                |
| SubcarrierBandwidth | uint32_t                       |                |
| numTones            | CSIDimension                   | uint16_t       |
| numTx               | CSIDimension                   | uint8_t        |
| numRx               | CSIDimension                   | uint8_t        |
| numESS              | CSIDimension                   | uint8_t        |
| numCSI              | CSIDimension                   | uint16_t       |
| ant_sel             | uint8_t                        |                |
| CSI                 | SignalMatrix[ccomplex[double]] | Cplx structure |
| Mag                 | SignalMatrix[double]           | Cplx structure |
| Phase               | SignalMatrix[double]           | Cplx structure |
| SubcarrierIndex     | vector[int16_t]                | Cplx structure |


#### Example 

```python
i = 0
frames = Picoscenes("rx_by_usrpN210.csi")
frame = frames.raw[i]  
if frame.get("LegacyCSI"):
    num_tones = frame.get("LegacyCSI").get("numTones")
```


### BasebandSignals(Optional[np.asarray])

```python
i = 0
frames = Picoscenes("rx_by_usrpN210.csi")
frame = frames.raw[i]  
baseband_signals = frame.get("BasebandSignals")
```


### PreEQSymbols(Optional[np.asarray])

```python
i = 0
frames = Picoscenes("rx_by_usrpN210.csi")
frame = frames.raw[i]  
pre_eq_symbols = frame.get("PreEQSymbols")
```


### MPDU(uint8_t)

```python
i = 0
frames = Picoscenes("rx_by_usrpN210.csi")
frame = frames.raw[i]  # get the first frame numtones
mpdu = frame.get("MPDU")
```

**If you have something dont know, you can check picoscenes.pyx or contact me.**
